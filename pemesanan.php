<?php 
require_once("koneksi.php");

if (isset($_POST['submit'])) {
	$kode_berlangganan = $_POST['kode_berlangganan'];
	$tgl_berlangganan = $_POST['tgl_berlangganan'];
	$id_tagihan = $_POST['id_tagihan'];
	$total_tagihan = $_POST['total_tagihan'];
	$bulan_tagihan = $_POST['bulan_tagihan'];
	$tgl_bayar = $_POST['tgl_bayar'];
	$id_pelanggan = $_POST['id_pelanggan'];
	$kode_paket = $_POST['kode_paket'];

	$sql_insert = "INSERT INTO berlangganan VALUES('$kode_berlangganan', '$tgl_berlangganan', '$id_tagihan', '$total_tagihan', '$bulan_tagihan', '$tgl_bayar', '$id_pelanggan', '$kode_paket')";
	mysqli_query($koneksi, $sql_insert);

	header("Location:produk.html");
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Pemesanan</title>
	<style type="text/css">
		body{
	font-family: calibri;
	background-image: url("tv.jpg");
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	background-attachment: fixed;
}
h1{
	text-align: center;
	color: white;
	font-size: 30px;
	margin-top: 2%;
	margin-bottom: 2%;
	letter-spacing: 10px;
}
form{
    position: relative;
    width: 25%;
    min-height: 350px;
    margin-left: 38%;
    margin-right: 35%;
    border-radius: 6px;
    background: rgba(0, 0, 0, 0.5);
    box-shadow: -1px -7px 20px 0px black;
    color: white;
}
table{
	margin-left: 10px;
	margin-right: 15px;
}
input{
	display: inline-block;
    width: 80%;
    border: 1px solid #8888;
    padding: 5px 20px 5px 20px;
    border-radius: 3px;

}
tr{
	position: relative;
    width: 100%;
    height: 50px;
}
button{
	margin-left: 5%;
	background: blue;
	border: none;
	color: white;
	font-weight: bold;
	letter-spacing: 3px;
	cursor: pointer;
	transition: background 0.5s ease-in-out;
	width: 90%;
	padding: 10px;
	margin-top: 5px;
	border-radius: 15px;
	box-shadow: 0 0 7px 0px blue;
	margin-bottom: 10px;
}
button:hover{
	color: black;
	background: skyblue;
	border-radius: 15px;
}
	</style>
</head>
<body>
	<h1>Pemesanan</h1>

	<form action="pemesanan.php" method="POST" class="badan">
		<table>
			<tr>
				<td>Kode Berlangganan</td>
				<td>:</td>
				<td>
					<input type="number" name="kode_berlangganan">
				</td>
			</tr>

			<tr>
				<td>Tanggal Berlangganan</td>
				<td>:</td>
				<td>
					<input type="date" name="tgl_berlangganan">
				</td>
			</tr>

			<tr>
				<td>ID Tagihan</td>
				<td>:</td>
				<td>
					<input type="number" name="id_tagihan">
				</td>
			</tr>

			<tr>
				<td>Total Tagihan</td>
				<td>:</td>
				<td>
					<input type="number" name="total_tagihan">
				</td>
			</tr>

			<tr>
				<td>Bulan Tagihan</td>
				<td>:</td>
				<td>
					<input type="text" name="bulan_tagihan">
				</td>
			</tr>

			<tr>
				<td>Tanggal Bayar</td>
				<td>:</td>
				<td>
					<input type="date" name="tgl_bayar">
				</td>
			</tr>

			<tr>
				<td>ID Pelanggan</td>
				<td>:</td>
				<td>
					<input type="number" name="id_pelanggan">
				</td>
			</tr>

			<tr>
				<td>Kode Paket</td>
				<td>:</td>
				<td>
					<input type="text" name="kode_paket">
				</td>
			</tr>
		</table>
		<button name="submit" type="submit">Pesan</button>
	</form>
</body>
</html>