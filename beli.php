<?php
session_start();
// mendapatkan kode paket dari url
$kode_paket = $_GET['kode_paket'];

// jika sudah ada produk itu dikeranjang, maka produk itu jumlahnya di +1
if(isset($_SESSION['keranjang'][$kode_paket])) {
    $_SESSION['keranjang'][$kode_paket]+=1;
}
// jika tidak, maka produk itu dianggap dibeli 1
else{
    $_SESSION['keranjang'][$kode_paket]=1;
}

// larikan ke halaman keranjang
echo "<script>alert('produk telah masuk ke keranjang belanja');</script>";
echo "<script>location='keranjang.php';</script>";
?>