<?php
require 'functions.php';

if(isset($_POST["regis"])){
    if(registrasi($_POST)>0){
        echo "<script>alert('user baru berhasil ditambahkan')</script>";
        header("Location:login.php");
    }
    else{
        echo mysqli_error($koneksi);
    }
}
?>
<?php include 'header.php'; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card p-4 mt-5 shadow-lg border-0">
                <h4 class="py-3 text-center">REGISTER</h4>
                <form action="" method="POST">
                    <div class="form-group">
                        <input type="text" placeholder="Name" name="nama" class="form-control rounded-pill" required="" autofocus="">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Username" name="username" class="form-control rounded-pill" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" name="password" class="form-control rounded-pill" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Konfirmasi Password" name="password2" class="form-control rounded-pill" required="">
                    </div>
                    <div class="tombol">
                        <input type="submit" value="Register" name="regis" class="btn btn-primary rounded-pill btn-block">
                    </div>
                </form>
                <p class="my-3 text-center small"><a href="login.php">Login Here</a></p>
                <p class="my-1 text-center small">Copyright &copy; 2021 All Rights Reserved by TVcable</p>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>