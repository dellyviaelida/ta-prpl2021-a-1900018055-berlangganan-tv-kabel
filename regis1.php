<?php 
require 'functions.php';

if (isset($_POST['lanjut'])) {
    $nama = $_POST['nama'];
	$jk = $_POST['jk'];
	$alamat = $_POST['alamat'];
	$telepon = $_POST['telepon'];
	
	$sql_insert = mysqli_query($koneksi, "INSERT INTO pelanggan VALUES( '', '$nama', 
                                                                        '$jk', 
                                                                        '$alamat', 
                                                                        '$telepon')");

	header("Location:regis.php");
}
?>

<?php include 'header.php'; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card p-4 mt-5 shadow-lg border-0">
                <h4 class="py-3 text-center">REGISTER</h4>
                <form action="" method="POST">
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" name="nama" class="form-control rounded-pill" required="" autofocus="">
                    </div>
                    <!-- <div class="form-group">
                        <input type="text" placeholder="Jenis Kelamin" name="jk" class="form-control rounded-pill" required="">
                        <input type="radio" name="jk" class="form-control" value="L">Male
                        <input type="radio" name="jk" class="form-control" value="P">Female
                        <select name="jk" id="">
                            <option value="">--Pilih Jenis Kelamin--</option>

                        </select>
                    </div> -->
                    <div class="btn-group btn-group-toggle mb-3" data-toggle="buttons">
                        <label class="btn btn-outline-info active">
                            <input type="radio" name="jk" id="option1" value="L" checked> Male
                        </label>
                        <label class="btn btn-outline-info">
                            <input type="radio" name="jk" id="option2" value="P"> Female
                        </label>
                    </div>
                    <div class="form-group">
                        <!-- <input type="text" placeholder="Alamat" name="alamat" class="form-control rounded-pill" required=""> -->
                        <textarea class="form-control" name="alamat" placeholder="Alamat" rows="2" required=""></textarea>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Telepon" name="telepon" class="form-control rounded-pill" required="">
                    </div>
                    <p class="my-1 text-center small float-left">Sudah punya akun? <a href="login.php">Login Here</a></p>
                    <div class="tombol float-right">
                        <input type="submit" value="Lanjutkan" name="lanjut" class="btn btn-primary rounded-pill">
                    </div>
                </form>
                <p class="my-1 mt-3 text-center small">Copyright &copy; 2021 All Rights Reserved by TVcable</p>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>